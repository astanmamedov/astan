﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RazorPages.Data;


namespace RazorPages.Pages
{
    public class IndexModel : PageModel
    {
        private readonly RazorPagesDbContext _context;
        public IndexModel(RazorPagesDbContext context)
        {
            _context = context;
        }
        public IList<Models.Student> student;
        public async Task OnGet()
        {
            student = await _context.Students.ToListAsync();
        }
        
    }
}
