using System;
using System.Collections.Generic;
using System.Linq;
using RazorPages.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using RazorPages.Data;


namespace RazorPages.Pages.Student
{ 
    public class IndexModel : PageModel
    {
        private readonly RazorPagesDbContext _context;
        public IndexModel(RazorPagesDbContext context)
        {
            _context = context;
        }

        
        public IList<Models.Student> Students { get; set; }

        [BindProperty(SupportsGet = true )]
        public string searchString { get; set; }
        public async Task OnGetAsync()
        {
            var stud = from m in _context.Students
                       select m;

            if(!String.IsNullOrEmpty(searchString))
            {
                stud = stud.Where(s => s.LastName.Contains(searchString));
            }
            Students = await stud.ToListAsync();
        }

        public void OnPostLess(double GPA)
        {
            var stud = from m in _context.Students
                       select m;

            if (!Double.IsNaN(GPA))
            {

                stud = stud.Where(s => s.GPA < GPA);

            }

            Students =  stud.ToList();
        }
        public void OnPostGreater(double GPA)
        {
            var stud = from m in _context.Students
                       select m;

            if (!Double.IsNaN(GPA))
            {

                stud = stud.Where(s => s.GPA > GPA);

            }

            Students = stud.ToList();
        }
    }
}